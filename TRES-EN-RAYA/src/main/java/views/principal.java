package views;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.plaf.basic.BasicBorders.RadioButtonBorder;

import java.awt.GridBagLayout;
import javax.swing.JToggleButton;
import java.awt.GridBagConstraints;
import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import dto.logica;
import dto.persona;

import java.awt.Panel;
import java.awt.Button;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JRadioButton;
import java.awt.Font;
import java.awt.Color;

public class principal extends JFrame {

	private JPanel contentPane;
	private JTextField n1;
	private JTextField n2;
	private JButton nueva;
	public persona p1[] = new persona[2];
	private logica l1 = new logica();
	private ButtonGroup gj2, gj1; 
	private JRadioButton tipo_h1, tipo_c1, tipo_h2, tipo_c2;
	private JToggleButton a1, a2, a3, b1, b2, b3, c1, c2, c3;
	private JLabel mensaje;
	private boolean acabada=false;
	private ArrayList<JToggleButton> botones = new ArrayList<JToggleButton>();
	private boolean inicio=false;
	
	public principal() {
		/**
		 * CONTENT PANE
		 */
		setTitle("Tres en raya");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 680, 480);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		/**
		 * TOGGLE BUTTONS DEL JUEGO
		 */
		a1 = new JToggleButton("");
		a1.setBackground(Color.BLACK);
		a1.setFont(new Font("Tahoma", Font.PLAIN, 60));
		a1.setBounds(10, 27, 121, 121);
		contentPane.add(a1);
		a1.setName("a1");
		l1.addNewButton(a1);
		a1.setSelected(true);
		a1.addActionListener(pulsar);
		botones.add(a1);
		
		b1 = new JToggleButton("");
		b1.setBackground(Color.BLACK);
		b1.setFont(new Font("Tahoma", Font.PLAIN, 60));
		b1.setBounds(10, 159, 121, 121);
		contentPane.add(b1);
		b1.setName("b1");
		l1.addNewButton(b1);
		b1.setSelected(true);
		b1.addActionListener(pulsar);
		botones.add(b1);
		
		c1 = new JToggleButton("");
		c1.setFont(new Font("Tahoma", Font.PLAIN, 60));
		c1.setBackground(Color.BLACK);
		c1.setBounds(10, 291, 121, 121);
		contentPane.add(c1);
		c1.setName("c1");
		l1.addNewButton(c1);
		c1.setSelected(true);
		c1.addActionListener(pulsar);
		botones.add(c1);
		
		c2 = new JToggleButton("");
		c2.setFont(new Font("Tahoma", Font.PLAIN, 60));
		c2.setBackground(Color.BLACK);
		c2.setBounds(141, 291, 121, 121);
		contentPane.add(c2);
		c2.setName("c2");
		l1.addNewButton(c2);
		c2.setSelected(true);
		c2.addActionListener(pulsar);
		botones.add(c2);
		
		b2 = new JToggleButton("");
		b2.setFont(new Font("Tahoma", Font.PLAIN, 60));
		b2.setBackground(Color.BLACK);
		b2.setBounds(141, 159, 121, 121);
		contentPane.add(b2);
		b2.setName("b2");
		l1.addNewButton(b2);
		b2.setSelected(true);
		b2.addActionListener(pulsar);
		botones.add(b2);
	
		a2 = new JToggleButton("");
		a2.setFont(new Font("Tahoma", Font.PLAIN, 60));
		a2.setBackground(Color.BLACK);
		a2.setBounds(141, 27, 121, 121);
		contentPane.add(a2);
		a2.setName("a2");
		l1.addNewButton(a2);
		a2.setSelected(true);
		a2.addActionListener(pulsar);
		botones.add(a2);
		
		a3 = new JToggleButton("");
		a3.setFont(new Font("Tahoma", Font.PLAIN, 60));
		a3.setBackground(Color.BLACK);
		a3.setBounds(272, 27, 121, 121);
		contentPane.add(a3);
		a3.setName("a3");
		l1.addNewButton(a3);
		a3.setSelected(true);
		a3.addActionListener(pulsar);
		botones.add(a3);
		
		b3 = new JToggleButton("");
		b3.setFont(new Font("Tahoma", Font.PLAIN, 60));
		b3.setBackground(Color.BLACK);
		b3.setBounds(272, 159, 121, 121);
		contentPane.add(b3);
		b3.setName("b3");
		l1.addNewButton(b3);
		b3.setSelected(true);
		b3.addActionListener(pulsar);
		botones.add(b3);
		
		c3 = new JToggleButton("");
		c3.setFont(new Font("Tahoma", Font.PLAIN, 60));
		c3.setBackground(Color.BLACK);
		c3.setBounds(272, 291, 121, 121);
		contentPane.add(c3);
		c3.setName("c3");
		l1.addNewButton(c3);
		c3.setSelected(true);
		c3.addActionListener(pulsar);
		botones.add(c3);
		/**
		 * BUTTON NUEVA PARTIDA
		 */
		nueva = new JButton("Nueva Partida");
		nueva.setBounds(492, 27, 121, 22);
		contentPane.add(nueva);
		nueva.addActionListener(nueva_partida);
		
		/**
		 * SELECCION DE OPCIONES J1
		 */
		mensaje = new JLabel("");
		mensaje.setForeground(Color.BLACK);
		mensaje.setBounds(419, 64, 222, 14);
		contentPane.add(mensaje);
		
		JLabel j1 = new JLabel("Jugador 1");
		j1.setBounds(419, 126, 73, 14);
		contentPane.add(j1);
		
		JLabel lbln1 = new JLabel("Nombre:");
		lbln1.setBounds(429, 151, 63, 14);
		contentPane.add(lbln1);
		
		n1 = new JTextField();
		n1.setBounds(487, 148, 86, 20);
		contentPane.add(n1);
		n1.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Tipo:");
		lblNewLabel.setBounds(427, 202, 46, 14);
		contentPane.add(lblNewLabel);
		
		tipo_h1 = new JRadioButton("Humano", true);
		tipo_h1.setSelected(true);
		tipo_h1.setBounds(468, 198, 89, 23);
		contentPane.add(tipo_h1);
		
		tipo_c1 = new JRadioButton("CPU", false);
		tipo_c1.setBounds(559, 198, 89, 23);
		contentPane.add(tipo_c1);
		
		gj1 = new ButtonGroup();
		gj1.add(tipo_h1);
		gj1.add(tipo_c1);
		
		/**
		 * SELECCION DE OPCIONES DE JUGADOR 2
		 */
		JLabel lblJugador = new JLabel("Jugador 2");
		lblJugador.setBounds(419, 291, 73, 14);
		contentPane.add(lblJugador);
		
		JLabel lbln1_1 = new JLabel("Nombre:");
		lbln1_1.setBounds(429, 316, 63, 14);
		contentPane.add(lbln1_1);
		
		n2 = new JTextField();
		n2.setColumns(10);
		n2.setBounds(487, 313, 86, 20);
		contentPane.add(n2);
		
		JLabel lblNewLabel_1 = new JLabel("Tipo:");
		lblNewLabel_1.setBounds(427, 367, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		tipo_h2 = new JRadioButton("Humano", true);
		tipo_h2.setSelected(true);
		tipo_h2.setBounds(468, 363, 89, 23);
		contentPane.add(tipo_h2);
		
		tipo_c2 = new JRadioButton("CPU", false);
		tipo_c2.setBounds(559, 363, 89, 23);
		contentPane.add(tipo_c2);
		
		gj2 = new ButtonGroup();
		gj2.add(tipo_h2);
		gj2.add(tipo_c2);
	}
	
	ActionListener nueva_partida = new ActionListener(){
		
		public void actionPerformed(ActionEvent e) {
			
			l1 = new logica();
			
			reinicio();
			
			p1[0] = new persona(1, n1.getText(), tipo_h1.isSelected());
			p1[1] = new persona(2, n2.getText(), tipo_h2.isSelected());
			
			mensaje.setText("Te toca tirar: "+ p1[0].getNombre());
			
			if (!p1[0].isHumano()) {
				acabada = l1.realizaTurno(p1, 0,null,mensaje);
			}
			
			
			
		}
		
	};
	
	ActionListener pulsar = new ActionListener(){
		
		public void actionPerformed(ActionEvent e) {
			JToggleButton j = (JToggleButton) e.getSource();
			
			if (inicio) {
				if (!acabada) {
					j.setSelected(true);
					acabada = l1.realizaTurno(p1, l1.getTurno()-1, j, mensaje);
				}else {
					j.setSelected(true);
					inicio=false;
				}
			}else {
				j.setSelected(true);
			}
			
		}
		
	};
	
	public void reinicio () {
		for (int i = 0; i < botones.size(); i++) {
			botones.get(i).setEnabled(true);
			botones.get(i).setSelected(true);
			botones.get(i).setText("");
			
			acabada = false;
			mensaje.setText(null);
			
			l1.addNewButton(botones.get(i));
			
			inicio = true;
		}
	}
	
	
}
