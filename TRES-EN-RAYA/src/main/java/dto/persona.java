package dto;

public class persona {

    protected int numJugador;
    protected String nombre;
    protected boolean humano;
    protected int tiradas;



    public persona() {
        numJugador=0;
        nombre="Manuel";
        humano=false;
        tiradas=0;
    }



    public persona(int numJugador, String nombre, boolean humano) {
        this.numJugador = numJugador;
        this.nombre = nombre;
        this.humano = humano;
        this.tiradas=0;
    }



    /**
     * 
     * @return the numJugador
     */
    public int getNumJugador() {
        return numJugador;
    }



    /**
     * 
     * @param numJugador the numJugador to set
     */
    public void setNumJugador(int numJugador) {
        this.numJugador = numJugador;
    }



    /**
     * @return the nombre
     */
    public String getNombre() {
        return nombre;
    }



    /**
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }



    /**
     * @return the humano
     */
    public boolean isHumano() {
        return humano;
    }



    /**
     * @param humano the humano to set
     */
    public void setHumano(boolean humano) {
        this.humano = humano;
    }



    /**
     * @return the tiradas
     */
    public int getTiradas() {
        return tiradas;
    }



    /**
     * @param tiradas the tiradas to set
     */
    public void setTiradas(int tiradas) {
        this.tiradas = tiradas;
    }






}
