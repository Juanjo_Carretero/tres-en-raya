package dto;

import java.util.ArrayList;
import java.util.HashMap;
import dto.persona;

import javax.swing.JLabel;
import javax.swing.JToggleButton;

public class logica {

	protected final char J1 = 'X';
    protected final char J2 = 'O';

    protected final char fichas[]= {'X','O','N'};
    protected final char fila_columna[][]= {{'a','b','c'},{'1','2','3'}};
    protected HashMap<JToggleButton, Character> tablero;
    protected int turno;
    protected boolean ganado;
    protected int fin;
    
    
    public logica() {
    	turno=1;
    	ganado=false;
    	fin=0;
    	tablero = new HashMap<JToggleButton, Character>();
	}

    public void addNewButton(JToggleButton b) {
        tablero.put(b, 'N');
    }

    public boolean realizaTurno(persona p[],int turno,JToggleButton b, JLabel j) {
        //Comprobar si jugador 1 o jugador 2
        char simb = 'N';
        
        do {
        	
        	if(p[turno].getNumJugador()==1)
        		simb=J1;
            else
            	simb=J2;
        	
            //Comprobar si humano o CPU
             if(p[turno].isHumano())
                 tiraHumano(simb,b);
             else
            	b = tiraCPU(simb);
             	p[turno].setTiradas(p[turno].getTiradas()+1);
        	
             if (ganado(b)) {
            	j.setText("Has ganado: " + p[turno].getNombre());
    			ganado = true;
             }
             
             if (turno==0) {
     			turno++;
     			this.turno=2;
     		}else {
     			turno--;
     			this.turno=1;
     		}
             
            if (!ganado) {
            	j.setText("Te toca tirar: " + p[turno].getNombre());
			}
            
            fin++;
		} while (!p[turno].isHumano() && !ganado && fin<9);
        
        if (fin==9&&!ganado) {
        	j.setText("Tablas");
		}
        return ganado||fin==9;
    }

    public void tiraHumano(char l,JToggleButton b) {
        tablero.replace(b, l);
        b.setText(String.valueOf(l));
        b.setEnabled(false);
    }

    public JToggleButton tiraCPU(char l) {
        //Sacar Casilla aleatoria libre
        ArrayList<JToggleButton> aux = new ArrayList<JToggleButton>();
        for(HashMap.Entry<JToggleButton, Character> entry : tablero.entrySet()) {
            if(entry.getValue() == 'N') {
                aux.add(entry.getKey());
            }
        }
        int op = aleatorio(0, aux.size()-1);
        tablero.replace(aux.get(op), l);
        
        aux.get(op).setText(String.valueOf(l));
        aux.get(op).setEnabled(false);
        
        return aux.get(op);
    }

    public int aleatorio(int min, int max) {
        int num = (int)(Math.random()*(max-min+1)+min);
        return num;
    }
    
    public int fila (String nomboton) {
    	char aux=nomboton.charAt(0);
    	
		for (int c = 0; c < 3; c++) {
			if (fila_columna[0][c]==aux) {
				return c;
			}	
		}
		return -1;
    }
    
    public int columna (String nomboton) {
    	char aux=nomboton.charAt(1);
    	
		for (int f = 0; f < 3; f++) {
			if (fila_columna[1][f]==aux) {
				return f;
			}	
		}
		return -1;
    }
    
    public JToggleButton fila_col(int fila, int col) {
    	String s = String.valueOf(fila_columna[0][fila])+String.valueOf(fila_columna[1][col]);
    	JToggleButton a = new JToggleButton();
    	
    	for(HashMap.Entry<JToggleButton, Character> entry : tablero.entrySet()) {
            if(entry.getKey().getName().equals(s)) {
                return entry.getKey();
            }
        }
    	
    	return a;
    }
    
    public boolean ganado(JToggleButton b) {
    	boolean ganado = false;
    	int fila=fila(b.getName());
    	int col=columna(b.getName());
    	
    	if ((col%2==0 && fila%2==0)||(col==1 && fila==1)) {
			ganado = mirarDiagonal(b);
		}
    	
    	if (!ganado){
			ganado = mirarHV(b);
		}
    	
    	return ganado;
    }
    
    public boolean mirarDiagonal(JToggleButton b) {
    	int fila=fila(b.getName());
    	int col=columna(b.getName());
    	char simbolo='N';
    	
    	if (fila == col) {
			//primera diagonal
    		simbolo = tablero.get(fila_col(0,0));
    		if ((tablero.get(fila_col(1,1)).equals(simbolo))&&(tablero.get(fila_col(2,2)).equals(simbolo))) {
				return true;
			}
		}
    	
    	if (fila + col == 2) {
    		simbolo = tablero.get(fila_col(0,2));
    		if ((tablero.get(fila_col(1,1)).equals(simbolo))&&(tablero.get(fila_col(2,0)).equals(simbolo))) {
				return true;
			}
		}
    	
    	return false;
    }
    
    public boolean mirarHV(JToggleButton b) {
    	int fila=fila(b.getName());
    	int col=columna(b.getName());
    	char simbolo='N';
    	
    	simbolo = tablero.get(b);
		if ((tablero.get(fila_col(fila,0)).equals(simbolo))&&(tablero.get(fila_col(fila,1)).equals(simbolo))&&(tablero.get(fila_col(fila,2)).equals(simbolo))) {
			return true;
		}
		
		if ((tablero.get(fila_col(0,col)).equals(simbolo))&&(tablero.get(fila_col(1,col)).equals(simbolo))&&(tablero.get(fila_col(2,col)).equals(simbolo))) {
			return true;
		}
    	
    	return false;
    }


    /**
     * GETTERS Y SETTERS TURNO
     * @return
     */
	public int getTurno() {
		return turno;
	}


	public void setTurno(int turno) {
		this.turno = turno;
	}
    
    

}
